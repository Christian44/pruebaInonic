import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { ModalController } from 'ionic-angular';
import { ModalPage } from './modal-page';
import { Publicaciones} from '../publicaciones/publicaciones';
import { usuarioEditar} from '../usuarioEditar/usuarioEditar';



@Component({
  selector: 'page-usuarios',
  templateUrl: 'usuarios.html',
})



export class Usuarios {

  posts: any;
  private searchQuery: string = '';
  private items: string[];
  constructor(public navCtrl: NavController, private http: Http) {

    this.initializeItems();

    this.http.get('http://127.0.0.1:8000/api/usuario')
    .map(res => res.json()).subscribe(data => {
        this.posts = data;
        console.log(this.posts);
    });

  }

  initializeItems() {
    this.items = this.posts;

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

    }
  }


  onClick(item)
  {

  this.navCtrl.push(usuarioEditar, {
  id: item.id
  });
  }

}