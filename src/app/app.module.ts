import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { HttpModule } from '@angular/http';

import { Noticias} from '../pages/noticias/noticias';
import { Publicaciones} from '../pages/publicaciones/publicaciones';
import { Proyectos } from '../pages/proyectos/proyectos';
import { Usuarios } from '../pages/usuarios/usuarios';
import { MenuInferior } from '../pages/menu-inferior/menu-inferior';
import { usuarioEditar} from '../pages/usuarioEditar/usuarioEditar';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    Noticias,
    Publicaciones,
    Proyectos,
    Usuarios,
    MenuInferior,
    usuarioEditar
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Noticias,
    Publicaciones,
    Proyectos,
    Usuarios,
    MenuInferior,
    usuarioEditar
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
